#include <iostream>
#include "functions.hpp"
#include <map>
#include <string>
#include <fstream>



namespace saw {
    void SEARCH(std::string Name) {
        std::ifstream in("map.txt");
        std::string key;
        int age;
        std::map<std::string, int>Map;
        while (!in.eof()) {
            in >> key;
            in >> age;
            Map[key] = age;
        }
        if (Map[Name] !=' ')
            std::cout << (*Map.find(Name)).first << " - " << (*Map.find(Name)).second << " years" << std::endl;
        else
            std::cout << "Name is not found!" << std::endl;       
    }
}

